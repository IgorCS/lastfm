package com.example.lastfmmusic.ui.topalbums

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lastfmmusic.data.api.artist.model.topalbummodels.TopAlbums
import com.example.lastfmmusic.data.db.entity.Resource
import com.example.lastfmmusic.data.repository.artist.remote.ArtistRemoteDataRepository

class ArtistTopAlbumsViewModel(private val artistRemoteDataRepository: ArtistRemoteDataRepository): ViewModel() {

    fun getArtistTopAlbums(artistName: String): MutableLiveData<Resource<TopAlbums>> {
        return artistRemoteDataRepository.getArtistTopAlbums(artistName)
    }
}