package com.example.lastfmmusic.ui.topalbums

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lastfmmusic.R
import com.example.lastfmmusic.data.api.artist.model.topalbummodels.ArtistTopAlbum
import com.squareup.picasso.Picasso

class ArtistTopAlbumsAdapter(private val onAlbumSelected: (ArtistTopAlbum, View) -> Unit) :
    RecyclerView.Adapter<ArtistTopAlbumsAdapter.TaskItemViewHolder>() {

    private val topAlbums = mutableListOf<ArtistTopAlbum>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val topAlbumItem = inflater.inflate(R.layout.top_album_item, parent, false)
        return TaskItemViewHolder(topAlbumItem)
    }

    override fun getItemCount(): Int {
        return topAlbums.size
    }

    override fun onBindViewHolder(holder: TaskItemViewHolder, position: Int) {
        holder.bind(topAlbums[position], onAlbumSelected)
    }

    fun addAll(list: List<ArtistTopAlbum>) {
        topAlbums.clear()
        topAlbums.addAll(list)
        notifyDataSetChanged()
    }

    class TaskItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val topAlbumImage: ImageView = view.findViewById(R.id.topAlbumImage)
        private val topAlbumName: TextView = view.findViewById(R.id.topAlbumName)

        fun bind(topAlbumsData: ArtistTopAlbum, listener: (ArtistTopAlbum, View) -> Unit) = with(itemView) {
            if (!TextUtils.isEmpty(topAlbumsData.name) && !TextUtils.isEmpty(topAlbumsData.image[2].imageUrl)) {
                Picasso.with(itemView.context).load(topAlbumsData.image[2].imageUrl).into(topAlbumImage)
                topAlbumName.text = topAlbumsData.name

                setOnClickListener { listener(topAlbumsData, itemView) }
            }
        }
    }
}