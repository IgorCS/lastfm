package com.example.lastfmmusic.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lastfmmusic.data.api.artist.model.searchmodels.ArtistMatches
import com.example.lastfmmusic.data.db.entity.Resource
import com.example.lastfmmusic.data.repository.artist.remote.ArtistRemoteDataRepository

class SearchArtistViewModel(private val artistRemoteDataRepository: ArtistRemoteDataRepository): ViewModel() {

    fun searchArtist(artistName: String): MutableLiveData<Resource<ArtistMatches>> {
        return artistRemoteDataRepository.searchArtist(artistName)
    }
}