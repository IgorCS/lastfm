package com.example.lastfmmusic.ui.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lastfmmusic.R
import com.example.lastfmmusic.data.db.entity.Track
import com.example.lastfmmusic.extentions.msTimeFormatter

class AlbumTracksAdapter : RecyclerView.Adapter<AlbumTracksAdapter.TrackItemViewHolder>() {

    private val tracks = mutableListOf<Track>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val trackItem = inflater.inflate(R.layout.track_item, parent, false)
        return TrackItemViewHolder(trackItem)
    }

    override fun getItemCount(): Int {
        return tracks.size
    }

    override fun onBindViewHolder(holder: TrackItemViewHolder, position: Int) {
        holder.bind(tracks[position])
    }

    fun addAll(list: List<Track>) {
        tracks.clear()
        tracks.addAll(list)
        notifyDataSetChanged()
    }

    class TrackItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val trackName: TextView = view.findViewById(R.id.trackName)
        private val trackDuration: TextView = view.findViewById(R.id.trackDuration)

        fun bind(track: Track) {
            trackName.text = track.name
            val duration = track.duration
            val d = duration.toLong()
            trackDuration.text = d.msTimeFormatter()
        }
    }
}