package com.example.lastfmmusic.ui.main

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lastfmmusic.R
import com.example.lastfmmusic.data.db.entity.Album
import com.squareup.picasso.Picasso

class MainAdapter(private val onAlbumSelected: (Album, View) -> Unit) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val albumList = mutableListOf<Album>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val topAlbumItem = inflater.inflate(R.layout.top_album_item, parent, false)
        return ViewHolder(topAlbumItem)
    }

    override fun getItemCount(): Int {
        return albumList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(albumList[position], onAlbumSelected)
    }

    fun addAll(list: List<Album>) {
        albumList.clear()
        albumList.addAll(list)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val albumImage: ImageView = view.findViewById(R.id.topAlbumImage)
        private val albumName: TextView = view.findViewById(R.id.topAlbumName)

        fun bind(topAlbumsData: Album, listener: (Album, View) -> Unit) = with(itemView) {
            if (!TextUtils.isEmpty(topAlbumsData.name) && !TextUtils.isEmpty(topAlbumsData.image)) {
                Picasso.with(itemView.context).load(topAlbumsData.image).into(albumImage)
                albumName.text = topAlbumsData.name

                setOnClickListener { listener(topAlbumsData, itemView) }
            }
        }
    }
}