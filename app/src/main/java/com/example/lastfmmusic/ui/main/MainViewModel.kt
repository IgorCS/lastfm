package com.example.lastfmmusic.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.lastfmmusic.data.db.entity.Album
import com.example.lastfmmusic.data.repository.album.local.AlbumLocalDataRepository

class MainViewModel(private val albumLocalDataRepository: AlbumLocalDataRepository) : ViewModel() {


    fun getFavoriteAlbums(): LiveData<MutableList<Album>>? {
        return albumLocalDataRepository.getFavoriteAlbums()
    }
}