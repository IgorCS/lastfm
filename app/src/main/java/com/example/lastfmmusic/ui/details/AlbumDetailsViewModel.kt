package com.example.lastfmmusic.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lastfmmusic.data.api.album.model.AlbumDetails
import com.example.lastfmmusic.data.db.entity.Album
import com.example.lastfmmusic.data.db.entity.Resource
import com.example.lastfmmusic.data.db.entity.Track
import com.example.lastfmmusic.data.repository.album.local.AlbumLocalDataRepository
import com.example.lastfmmusic.data.repository.album.remote.AlbumRemoteDataRepository

class AlbumDetailsViewModel(private val albumRemoteDataRepository: AlbumRemoteDataRepository, private val albumLocalDataRepository: AlbumLocalDataRepository): ViewModel() {

    private val _isFavorite = MutableLiveData<Boolean>()
    val isFavorite: LiveData<Boolean>
        get() = _isFavorite

    fun getAlbumDetails(albumName: String, artistName: String): MutableLiveData<Resource<AlbumDetails>> {
        _isFavorite.postValue(false)
        return albumRemoteDataRepository.getAlbumDetails(albumName, artistName)
    }

    suspend fun saveAlbumToFavorites(album: Album): Long {
        _isFavorite.postValue(true)
        return albumLocalDataRepository.saveAlbumToFavorites(album)
    }

    suspend fun saveTracksToFavorites(trackList: List<Track>) {
        albumLocalDataRepository.saveTracksToFavorites(trackList)
    }

    suspend fun removeAlbumFromFavorites(album: Album, trackList: List<Track>) {
        _isFavorite.postValue(false)
        albumLocalDataRepository.removeAlbumFromFavorites(album, trackList)
    }

    suspend fun getLocalAlbumTracks(albumId: Long): List<Track> {
        return albumLocalDataRepository.getLocalAlbumTracks(albumId)
    }

    suspend fun getAlbum(mbid: String): Album? {
        val album = albumLocalDataRepository.getAlbum(mbid)
        val isFavorite = album != null
        _isFavorite.postValue(isFavorite)
        return album
    }
}