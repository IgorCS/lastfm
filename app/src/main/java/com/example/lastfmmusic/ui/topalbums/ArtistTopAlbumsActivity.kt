package com.example.lastfmmusic.ui.topalbums

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lastfmmusic.*
import com.example.lastfmmusic.data.api.artist.model.topalbummodels.ArtistTopAlbum
import com.example.lastfmmusic.data.db.entity.Status
import com.example.lastfmmusic.extentions.startNewActivity
import com.example.lastfmmusic.ui.details.AlbumDetailsActivity
import kotlinx.android.synthetic.main.activity_artist_top_albums.*
import kotlinx.android.synthetic.main.progress_bar.*
import org.koin.android.viewmodel.ext.android.viewModel

class ArtistTopAlbumsActivity : AppCompatActivity() {

    private lateinit var viewAdapter: ArtistTopAlbumsAdapter
    private val artistTopAlbumsViewModel: ArtistTopAlbumsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_top_albums)
        val artistName = intent?.extras?.getString(ARTIST_NAME)
        title = resources.getString(R.string.artist_top_albums_title, artistName)
        setupRecycler()
        artistName?.let {
            getTopAlbums(it)
        }
    }

    private fun getTopAlbums(artist: String) {
        artistTopAlbumsViewModel.getArtistTopAlbums(artist).observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    topAlbumsRecyclerView.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    topAlbumsRecyclerView.visibility = View.VISIBLE
                    it.data?.topAlbums?.let {
                            topAlbums -> viewAdapter.addAll(topAlbums)
                    } ?: ArrayList<ArtistTopAlbum>()
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setupRecycler() {
        viewAdapter = ArtistTopAlbumsAdapter { topAlbumsData, _ ->
            navigateToAlbumDetailsScreen(topAlbumsData)
        }
        val viewManager = LinearLayoutManager(this)

        topAlbumsRecyclerView.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    private fun navigateToAlbumDetailsScreen(artistTopAlbum: ArtistTopAlbum) {
        val extras = Bundle()
        extras.putString(ALBUM_NAME, artistTopAlbum.name)
        extras.putString(ARTIST_NAME, artistTopAlbum.artist.name)
        extras.putString(ALBUM_MBID, artistTopAlbum.mbid)
        startNewActivity(AlbumDetailsActivity::class.java, extras)
    }
}
