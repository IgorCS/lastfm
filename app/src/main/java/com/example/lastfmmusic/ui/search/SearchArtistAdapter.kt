package com.example.lastfmmusic.ui.search

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lastfmmusic.R
import com.example.lastfmmusic.data.api.artist.model.searchmodels.ArtistData
import com.squareup.picasso.Picasso

class SearchArtistAdapter(private val onAlbumSelected: (ArtistData, View) -> Unit) :
        RecyclerView.Adapter<SearchArtistAdapter.ViewHolder>() {

    private val topAlbums = mutableListOf<ArtistData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val topAlbumItem = inflater.inflate(R.layout.top_album_item, parent, false)
        return ViewHolder(topAlbumItem)
    }

    override fun getItemCount(): Int {
        return topAlbums.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(topAlbums[position], onAlbumSelected)
    }

    fun addAll(list: List<ArtistData>) {
        topAlbums.clear()
        topAlbums.addAll(list)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val topAlbumImage: ImageView = view.findViewById(R.id.topAlbumImage)
        private val topAlbumName: TextView = view.findViewById(R.id.topAlbumName)

        fun bind(artistData: ArtistData, listener: (ArtistData, View) -> Unit) = with(itemView) {
            if (!TextUtils.isEmpty(artistData.name) && !TextUtils.isEmpty(artistData.image[2].imageUrl)) {
                Picasso.with(itemView.context).load(artistData.image[2].imageUrl).into(topAlbumImage)
                topAlbumName.text = artistData.name

                setOnClickListener { listener(artistData, itemView) }
            }
        }
    }
}