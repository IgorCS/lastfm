package com.example.lastfmmusic.ui.search

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.HIDE_NOT_ALWAYS
import android.widget.SearchView.OnQueryTextListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lastfmmusic.ARTIST_NAME
import com.example.lastfmmusic.R
import com.example.lastfmmusic.data.api.artist.model.searchmodels.ArtistData
import com.example.lastfmmusic.data.db.entity.Status
import com.example.lastfmmusic.extentions.hideKeyboard
import com.example.lastfmmusic.extentions.startNewActivity
import com.example.lastfmmusic.ui.topalbums.ArtistTopAlbumsActivity
import kotlinx.android.synthetic.main.activity_search_artist.*
import kotlinx.android.synthetic.main.progress_bar.*
import org.koin.android.viewmodel.ext.android.viewModel


class SearchArtistActivity : AppCompatActivity() {

    private lateinit var viewAdapter: SearchArtistAdapter
    private lateinit var searchMenuItem: MenuItem
    private val searchArtistViewModel: SearchArtistViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_artist)
        setTitle(R.string.search)
        setupRecycler()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        searchMenuItem = menu.findItem(R.id.search).apply {
            setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                    item?.actionView?.requestFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.toggleSoftInput(0, HIDE_NOT_ALWAYS)
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                    return true
                }

            })
        }

        val searchView = searchMenuItem.actionView as android.widget.SearchView
        searchView.apply {
            queryHint = getString(R.string.search_artist)
            setIconifiedByDefault(false)
        }
        searchView.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                getArtistList(newText)
                return true
            }
        })
        return true
    }

    private fun getArtistList(artist: String) {
        searchArtistViewModel.searchArtist(artist).observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    searchArtistRecyclerView.visibility = View.GONE
                    noResultsMessage.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    searchArtistRecyclerView.visibility = View.VISIBLE
                    it.data?.artistMatches?.artistList?.let { topAlbums -> viewAdapter.addAll(topAlbums) }
                            ?: ArrayList<ArtistData>()
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    noResultsMessage.visibility = View.VISIBLE
                    Toast.makeText(this, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setupRecycler() {
        viewAdapter = SearchArtistAdapter { artistData, _ ->
            searchMenuItem.collapseActionView()
            hideKeyboard()
            navigateToAlbumsScreen(artistData)
        }
        val viewManager = LinearLayoutManager(this)

        searchArtistRecyclerView.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    private fun navigateToAlbumsScreen(artistData: ArtistData) {
        val extras = Bundle()
        extras.putString(ARTIST_NAME, artistData.name)
        startNewActivity(ArtistTopAlbumsActivity::class.java, extras)
    }
}
