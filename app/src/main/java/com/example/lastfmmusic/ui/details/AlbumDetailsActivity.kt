package com.example.lastfmmusic.ui.details

import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lastfmmusic.ALBUM_MBID
import com.example.lastfmmusic.ALBUM_NAME
import com.example.lastfmmusic.ARTIST_NAME
import com.example.lastfmmusic.R
import com.example.lastfmmusic.data.api.album.model.AlbumDetails
import com.example.lastfmmusic.data.db.entity.Album
import com.example.lastfmmusic.data.db.entity.Status
import com.example.lastfmmusic.data.db.entity.Track
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_album_details.*
import kotlinx.android.synthetic.main.progress_bar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel


class AlbumDetailsActivity : AppCompatActivity() {

    private val albumDetailsViewModel: AlbumDetailsViewModel by viewModel()
    private lateinit var tracksAdapter: AlbumTracksAdapter
    private val mainJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + mainJob)

    private var album: Album? = null
    private lateinit var trackList: List<Track>
    private var mbid: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album_details)
        setTitle(R.string.album_details_title)
        val albumName = intent.getStringExtra(ALBUM_NAME)
        val artistName = intent.getStringExtra(ARTIST_NAME)
        mbid = intent.getStringExtra(ALBUM_MBID)

        setupRecycler()
        getAlbumData(albumName, artistName)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.album_details_menu, menu)

        menu?.findItem(R.id.saveAlbum).also { saveAlbumItem ->
            albumDetailsViewModel.isFavorite.observe(this, Observer { isFavorite ->
                if (isFavorite) {
                    menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite)
                } else {
                    menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border)
                }

                saveAlbumItem?.setOnMenuItemClickListener {
                    when {
                        isFavorite -> {
                            removeAlbumFromFavorite()
                        }
                        else -> {
                            saveAlbumDataToFavorites()
                        }
                    }
                    false
                }
            })
        }
        return true
    }

    private fun saveAlbumDataToFavorites() {
        if (album?.mbid == null) {
            Snackbar.make(albumDetailsLayout, R.string.cannot_save_album_message, Snackbar.LENGTH_LONG).show()
        } else {
            coroutineScope.launch {
                album?.isFavorite = true

                val id = albumDetailsViewModel.saveAlbumToFavorites(album!!)
                trackList.forEach { track ->
                    track.albumId = id
                }
                albumDetailsViewModel.saveTracksToFavorites(trackList)
            }
            Snackbar.make(albumDetailsLayout, R.string.save_album_snack_bar, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun removeAlbumFromFavorite() {
        coroutineScope.launch {
            album = albumDetailsViewModel.getAlbum(mbid!!)

            album?.let {
                trackList = albumDetailsViewModel.getLocalAlbumTracks(it.id)
                albumDetailsViewModel.removeAlbumFromFavorites(it, trackList)
            }
        }
        Snackbar.make(albumDetailsLayout, R.string.remove_album_snack_bar, Snackbar.LENGTH_LONG).show()
    }


    private fun getRemoteAlbumDetails(albumName: String, artistName: String) {
        albumDetailsViewModel.getAlbumDetails(albumName, artistName).observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    albumTracksRecycleView.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    albumTracksRecycleView.visibility = View.VISIBLE

                    it.data?.let { albumDetails ->
                        setupAlbumLocalData(albumDetails)
                        setupTrackListData(albumDetails)
                        setupAlbumDetailsUI(album)
                    } ?: ArrayList<Track>()
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setupAlbumLocalData(albumDetails: AlbumDetails) {
        album = Album(
            albumDetails.artist,
            albumDetails.name,
            albumDetails.mbid,
            albumDetails.image[3].imageUrl,
            false
        )
    }

    private fun setupTrackListData(albumDetails: AlbumDetails) {
        trackList = albumDetails.tracks.trackList
    }

    private fun setupRecycler() {
        tracksAdapter = AlbumTracksAdapter()
        val viewManager = LinearLayoutManager(this)

        val itemDecor = DividerItemDecoration(albumTracksRecycleView.context, RecyclerView.VERTICAL)
        albumTracksRecycleView.apply {
            layoutManager = viewManager
            adapter = tracksAdapter
            addItemDecoration(itemDecor)
        }
    }

    private fun getAlbumData(albumName: String, artistName: String) {
        coroutineScope.launch {
            mbid?.let { mbid ->
                album = albumDetailsViewModel.getAlbum(mbid)
                album?.let {
                    trackList = albumDetailsViewModel.getLocalAlbumTracks(it.id)
                    setupAlbumDetailsUI(album)
                } ?: getRemoteAlbumDetails(albumName, artistName)
            } ?: getRemoteAlbumDetails(albumName, artistName)
        }
    }

    private fun setupAlbumDetailsUI(album: Album?) {
        if (!TextUtils.isEmpty(album?.image)) {
            Picasso.with(this).load(album?.image).into(albumDetailsImage)
        }
        albumDetailsArtist.text = album?.artistName
        albumDetailsName.text = album?.name
        tracksAdapter.addAll(trackList)
    }
}
