package com.example.lastfmmusic.ui.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lastfmmusic.ALBUM_MBID
import com.example.lastfmmusic.ALBUM_NAME
import com.example.lastfmmusic.ARTIST_NAME
import com.example.lastfmmusic.R
import com.example.lastfmmusic.data.db.entity.Album
import com.example.lastfmmusic.extentions.startNewActivity
import com.example.lastfmmusic.ui.details.AlbumDetailsActivity
import com.example.lastfmmusic.ui.search.SearchArtistActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var viewAdapter: MainAdapter
    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecycler()
        getFavoriteAlbums()
        openSearchScreenFAB.setOnClickListener {
            navigateToSearchPage()
        }
    }

    private fun setupRecycler() {
        viewAdapter = MainAdapter { topAlbumsData, _ ->
            navigateToAlbumDetailsScreen(topAlbumsData)
        }
        val viewManager = LinearLayoutManager(this)

        albumsRecyclerView.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    private fun getFavoriteAlbums() {
        mainViewModel.getFavoriteAlbums()?.observe(this@MainActivity, Observer {
            viewAdapter.addAll(it)
            if (it.isEmpty()) {
                noAlbumsMessage.visibility = View.VISIBLE
            } else {
                noAlbumsMessage.visibility = View.GONE
            }
        })
    }

    private fun navigateToAlbumDetailsScreen(album: Album) {
        val extras = Bundle()
        extras.putString(ALBUM_NAME, album.name)
        extras.putString(ARTIST_NAME, album.artistName)
        extras.putString(ALBUM_MBID, album.mbid)
        startNewActivity(AlbumDetailsActivity::class.java, extras)
    }

    private fun navigateToSearchPage() {
        startNewActivity(SearchArtistActivity::class.java)
    }
}
