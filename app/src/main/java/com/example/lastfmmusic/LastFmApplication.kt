package com.example.lastfmmusic

import android.app.Application
import com.example.lastfmmusic.di.appModules
import org.koin.android.ext.android.startKoin

class LastFmApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, appModules)
    }
}