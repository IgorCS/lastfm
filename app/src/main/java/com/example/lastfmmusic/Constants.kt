package com.example.lastfmmusic

const val ALBUM_MBID = "album_mbid"
const val ALBUM_NAME = "album_name"
const val ARTIST_NAME = "artist_name"
const val TIMER_STRING_FORMAT = "%02d:%02d"
