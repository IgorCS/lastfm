package com.example.lastfmmusic.data.api.artist.model.searchmodels

import com.example.lastfmmusic.data.api.album.model.AlbumImage

data class ArtistData(
    val name: String,
    val listeners: String,
    val image: List<AlbumImage>
)