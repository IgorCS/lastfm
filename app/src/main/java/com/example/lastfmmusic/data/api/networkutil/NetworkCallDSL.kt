package com.example.lastfmmusic.data.api.networkutil

import androidx.lifecycle.MutableLiveData
import com.example.lastfmmusic.data.db.entity.Resource
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response

class CallHandler<RESPONSE : Any, DATA: Any> {
    lateinit var client: Deferred<Response<RESPONSE>>

    @Suppress("UNCHECKED_CAST")
    fun makeCall() : MutableLiveData<Resource<DATA>> {
        val result = MutableLiveData<Resource<DATA>>()
        result.postValue(Resource.loading(null))

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = client.awaitResult().getOrThrow() as DataResponse<DATA>

                withContext(Dispatchers.Main) {
                    result.value = Resource.success(response.retrieveData())
                }
            } catch (e: Throwable) {
                withContext(Dispatchers.Main) {
                    if (e is HttpException)
                        result.value = Resource.error("${e.message} | code ${e.response().code()}",
                            null)
                    else
                        result.value = Resource.error("${e.message}", null)
                }
                e.printStackTrace()
            }
        }
        return result
    }
}

fun <RESPONSE: DataResponse<*>, DATA: Any> networkCall(block: CallHandler<RESPONSE, DATA>.() -> Unit): MutableLiveData<Resource<DATA>>
    = CallHandler<RESPONSE, DATA>().apply(block).makeCall()

    interface DataResponse<T> {
    fun retrieveData(): T
}
