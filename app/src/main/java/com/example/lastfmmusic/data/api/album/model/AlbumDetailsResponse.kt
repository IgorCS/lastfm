package com.example.lastfmmusic.data.api.album.model

import com.example.lastfmmusic.data.api.networkutil.DataResponse
import com.google.gson.annotations.SerializedName

class AlbumDetailsResponse (
    @SerializedName("album")
    val albumDetails: AlbumDetails
): DataResponse<AlbumDetails> {
    override fun retrieveData(): AlbumDetails = albumDetails
}