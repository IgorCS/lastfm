package com.example.lastfmmusic.data.api.album.model

import com.google.gson.annotations.SerializedName

data class AlbumImage(
    @SerializedName("#text")
    val imageUrl: String,
    val size: String
)