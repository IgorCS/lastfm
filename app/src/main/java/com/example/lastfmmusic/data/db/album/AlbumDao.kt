package com.example.lastfmmusic.data.db.album

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.lastfmmusic.data.db.entity.Album

@Dao
interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAlbum(album: Album): Long

    @Delete
    fun removeAlbum(album: Album)

    @Query("SELECT * from album")
    fun getAllAlbums(): LiveData<MutableList<Album>>

    @Query("SELECT * from album where mbid=:mbid")
    fun getAlbum(mbid: String): Album?
}