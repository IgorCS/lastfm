package com.example.lastfmmusic.data.api.album.model

import com.example.lastfmmusic.data.db.entity.Track
import com.google.gson.annotations.SerializedName

data class Tracks (
    @SerializedName("track")
    val trackList: List<Track>
)