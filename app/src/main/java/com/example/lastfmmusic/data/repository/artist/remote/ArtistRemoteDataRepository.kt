package com.example.lastfmmusic.data.repository.artist.remote

import androidx.lifecycle.MutableLiveData
import com.example.lastfmmusic.data.api.artist.ArtistApi
import com.example.lastfmmusic.data.api.artist.model.searchmodels.ArtistMatches
import com.example.lastfmmusic.data.api.artist.model.searchmodels.ArtistSearchResponse
import com.example.lastfmmusic.data.api.artist.model.topalbummodels.ArtistTopAlbumsResponse
import com.example.lastfmmusic.data.api.artist.model.topalbummodels.TopAlbums
import com.example.lastfmmusic.data.api.networkutil.networkCall
import com.example.lastfmmusic.data.db.entity.Resource
import com.example.lastfmmusic.di.ApiProperties.API_KEY

class ArtistRemoteDataRepository(private val artistApi: ArtistApi) {

    fun getArtistTopAlbums(artistName: String): MutableLiveData<Resource<TopAlbums>> =
            networkCall<ArtistTopAlbumsResponse, TopAlbums> {
                client = artistApi.getArtistTopAlbumsAsync("artist.gettopalbums", artistName, API_KEY)
            }

    fun searchArtist(artistName: String): MutableLiveData<Resource<ArtistMatches>> =
            networkCall<ArtistSearchResponse, ArtistMatches> {
                client = artistApi.searchArtistAsync("artist.search", artistName, API_KEY)
            }
}