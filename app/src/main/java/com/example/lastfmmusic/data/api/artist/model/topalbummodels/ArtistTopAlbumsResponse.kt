package com.example.lastfmmusic.data.api.artist.model.topalbummodels

import com.example.lastfmmusic.data.api.networkutil.DataResponse
import com.google.gson.annotations.SerializedName

data class ArtistTopAlbumsResponse(
    @SerializedName("topalbums")
    val topAlbums: TopAlbums
) : DataResponse<TopAlbums> {
    override fun retrieveData(): TopAlbums = topAlbums
}