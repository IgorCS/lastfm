package com.example.lastfmmusic.data.api.artist.model.searchmodels

import com.example.lastfmmusic.data.api.networkutil.DataResponse
import com.google.gson.annotations.SerializedName

data class ArtistSearchResponse(
        @SerializedName("results")
        val item: ArtistMatches
): DataResponse<ArtistMatches> {
    override fun retrieveData(): ArtistMatches = item
}