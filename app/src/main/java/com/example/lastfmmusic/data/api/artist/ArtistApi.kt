package com.example.lastfmmusic.data.api.artist

import com.example.lastfmmusic.data.api.artist.model.searchmodels.ArtistSearchResponse
import com.example.lastfmmusic.data.api.artist.model.topalbummodels.ArtistTopAlbumsResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ArtistApi {

    @GET("/2.0/")
    fun searchArtistAsync(
            @Query("method") method: String,
            @Query("artist") artist: String,
            @Query("api_key") apiKey: String): Deferred<Response<ArtistSearchResponse>>

    @GET("/2.0/")
    fun getArtistTopAlbumsAsync(
            @Query("method") method: String,
            @Query("artist") artist: String,
            @Query("api_key") apiKey: String): Deferred<Response<ArtistTopAlbumsResponse>>
}