package com.example.lastfmmusic.data.api.artist.model.topalbummodels

data class AlbumArtistData (
    val name: String,
    val mbid: String,
    val url: String
)