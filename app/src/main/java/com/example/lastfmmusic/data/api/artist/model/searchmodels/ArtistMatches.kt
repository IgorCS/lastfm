package com.example.lastfmmusic.data.api.artist.model.searchmodels

import com.google.gson.annotations.SerializedName

data class ArtistMatches(
        @SerializedName("artistmatches")
        val artistMatches: ArtistList
)