package com.example.lastfmmusic.data.db.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "album")
data class Album(

    val artistName: String,

    val name: String,

    val mbid: String?,

    val image: String,

    var isFavorite: Boolean
) {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id: Long = 0
}