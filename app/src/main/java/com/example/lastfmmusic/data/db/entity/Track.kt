package com.example.lastfmmusic.data.db.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "track")
data class Track(
    var name: String,
    var duration: String,

    @ForeignKey(
        entity = Album::class,
        parentColumns = ["id"],
        childColumns = ["albumId"],
        onDelete = ForeignKey.CASCADE
    )
    var albumId: Long
) {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id: Long = 0
}