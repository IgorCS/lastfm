package com.example.lastfmmusic.data.api.album

import com.example.lastfmmusic.data.api.album.model.AlbumDetailsResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface AlbumApi {

    @GET("/2.0/")
    fun getAlbumDetailsAsync(
            @Query("method") method: String,
            @Query("api_key") apiKey: String,
            @Query("album") albumName: String,
            @Query("artist") artistName: String
    ): Deferred<Response<AlbumDetailsResponse>>
}