package com.example.lastfmmusic.data.api.artist.model.topalbummodels

import com.example.lastfmmusic.data.api.album.model.AlbumImage

data class ArtistTopAlbum (
    val name: String?,
    val mbid: String,
    val url: String,
    val artist: AlbumArtistData,
    val image: List<AlbumImage>
)