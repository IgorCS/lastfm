package com.example.lastfmmusic.data.api.album.model

data class AlbumDetails(
    val name: String,
    val artist: String,
    val mbid: String?,
    val image: List<AlbumImage>,
    val tracks: Tracks
)