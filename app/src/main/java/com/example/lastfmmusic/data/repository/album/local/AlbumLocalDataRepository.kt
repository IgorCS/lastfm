package com.example.lastfmmusic.data.repository.album.local

import androidx.lifecycle.LiveData
import com.example.lastfmmusic.data.db.album.AlbumDao
import com.example.lastfmmusic.data.db.album.TrackDao
import com.example.lastfmmusic.data.db.entity.Album
import com.example.lastfmmusic.data.db.entity.Track
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AlbumLocalDataRepository(private val albumDao: AlbumDao, private val trackDao: TrackDao) {

    fun getFavoriteAlbums(): LiveData<MutableList<Album>>? {
        return albumDao.getAllAlbums()
    }

    suspend fun saveAlbumToFavorites(album: Album): Long {
        return withContext(Dispatchers.IO) {
            albumDao.saveAlbum(album)
        }
    }

    suspend fun saveTracksToFavorites(trackList: List<Track>) {
        return withContext(Dispatchers.IO) {
            trackDao.insertTracks(trackList)
        }
    }

    suspend fun removeAlbumFromFavorites(album: Album, trackList: List<Track>) {
        return withContext(Dispatchers.IO) {
            albumDao.removeAlbum(album)
            trackDao.deleteTracks(trackList)
        }
    }

    suspend fun getLocalAlbumTracks(albumId: Long): List<Track> {
        return withContext(Dispatchers.IO) {
            trackDao.getLocalAlbumTracks(albumId)
        }
    }

    suspend fun getAlbum(mbid: String): Album? {
        return withContext(Dispatchers.IO) {
            albumDao.getAlbum(mbid)
        }
    }
}