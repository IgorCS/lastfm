package com.example.lastfmmusic.data.api.artist.model.topalbummodels

import com.example.lastfmmusic.data.api.networkutil.DataResponse
import com.google.gson.annotations.SerializedName

class TopAlbums(
    @SerializedName("album")
    val topAlbums: List<ArtistTopAlbum>
) : DataResponse<List<ArtistTopAlbum>> {

    override fun retrieveData(): List<ArtistTopAlbum> {
        return topAlbums
    }
}