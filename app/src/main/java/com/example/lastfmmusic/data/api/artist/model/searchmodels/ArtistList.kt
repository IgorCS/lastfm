package com.example.lastfmmusic.data.api.artist.model.searchmodels

import com.google.gson.annotations.SerializedName

data class ArtistList(
    @SerializedName("artist")
    val artistList: List<ArtistData>
)