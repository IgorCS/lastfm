package com.example.lastfmmusic.data.repository.album.remote

import androidx.lifecycle.MutableLiveData
import com.example.lastfmmusic.data.api.album.AlbumApi
import com.example.lastfmmusic.data.api.album.model.AlbumDetails
import com.example.lastfmmusic.data.api.album.model.AlbumDetailsResponse
import com.example.lastfmmusic.data.api.networkutil.networkCall
import com.example.lastfmmusic.data.db.entity.Resource
import com.example.lastfmmusic.di.ApiProperties.API_KEY


class AlbumRemoteDataRepository(private val albumApi: AlbumApi) {

    fun getAlbumDetails(albumName: String, artistName: String): MutableLiveData<Resource<AlbumDetails>> =
        networkCall<AlbumDetailsResponse, AlbumDetails> {
            client = albumApi.getAlbumDetailsAsync("album.getInfo", API_KEY, albumName, artistName)
        }
}
