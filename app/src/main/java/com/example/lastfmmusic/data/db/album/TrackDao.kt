package com.example.lastfmmusic.data.db.album

import androidx.room.*
import com.example.lastfmmusic.data.db.entity.Track

@Dao
interface TrackDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTracks(tracks: List<Track>)

    @Delete
    fun deleteTracks(tracks: List<Track>)

    @Query("SELECT * from track WHERE albumId=:albumId")
    fun getLocalAlbumTracks(albumId: Long): List<Track>
}