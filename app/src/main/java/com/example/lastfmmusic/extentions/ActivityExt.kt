package com.example.lastfmmusic.extentions

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

fun <T : AppCompatActivity> Activity.startNewActivity(newActivity: Class<T>, extras: Bundle? = null) {
    val newActivityIntent = Intent(this, newActivity)

    if (extras != null) {
        newActivityIntent.putExtras(extras)
    }
    startActivity(newActivityIntent)
}

fun Activity.hideKeyboard() {
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
}