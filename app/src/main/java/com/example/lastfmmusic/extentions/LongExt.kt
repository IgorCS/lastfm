package com.example.lastfmmusic.extentions

import com.example.lastfmmusic.TIMER_STRING_FORMAT
import java.util.concurrent.TimeUnit

fun Long.msTimeFormatter(): String {
    return String.format(
        TIMER_STRING_FORMAT,
        TimeUnit.SECONDS.toMinutes(this) - TimeUnit.HOURS.toMinutes(
            TimeUnit.SECONDS.toHours(this)
        ),
        TimeUnit.SECONDS.toSeconds(this) - TimeUnit.MINUTES.toSeconds(
            TimeUnit.SECONDS.toMinutes(this)
        )
    )
}