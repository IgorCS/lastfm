package com.example.lastfmmusic.di

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.lastfmmusic.data.db.album.AlbumDao
import com.example.lastfmmusic.data.db.album.TrackDao
import com.example.lastfmmusic.data.db.entity.Album
import com.example.lastfmmusic.data.db.entity.Track
import org.koin.dsl.module.module

val roomModule = module {
    single { provideDatabaseInstance(get()) }
    single { provideAlbumDao(get()) }
    single { provideTrackDao(get()) }
}

private object DatabaseProperties {
    const val DATABASE_VERSION = 2
}

private fun provideDatabaseInstance(applicationContext: Context): AppDatabase {
    return AppDatabase.getAppDataBase(applicationContext)
}

private fun provideAlbumDao(appDatabase: AppDatabase): AlbumDao {
    return appDatabase.albumDao()
}

private fun provideTrackDao(appDatabase: AppDatabase): TrackDao {
    return appDatabase.trackDao()
}


@Database(entities = [Album::class, Track::class], version = DatabaseProperties.DATABASE_VERSION)
abstract class AppDatabase : RoomDatabase() {
    abstract fun albumDao(): AlbumDao
    abstract fun trackDao(): TrackDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room
                        .databaseBuilder(context.applicationContext, AppDatabase::class.java, "lastFmDb")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE!!
        }
    }
}