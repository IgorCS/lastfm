package com.example.lastfmmusic.di

import com.example.lastfmmusic.data.api.album.AlbumApi
import com.example.lastfmmusic.data.api.artist.ArtistApi
import com.example.lastfmmusic.data.db.album.AlbumDao
import com.example.lastfmmusic.data.db.album.TrackDao
import com.example.lastfmmusic.data.repository.album.local.AlbumLocalDataRepository
import com.example.lastfmmusic.data.repository.album.remote.AlbumRemoteDataRepository
import com.example.lastfmmusic.data.repository.artist.remote.ArtistRemoteDataRepository
import com.example.lastfmmusic.ui.details.AlbumDetailsViewModel
import com.example.lastfmmusic.ui.main.MainViewModel
import com.example.lastfmmusic.ui.search.SearchArtistViewModel
import com.example.lastfmmusic.ui.topalbums.ArtistTopAlbumsViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import retrofit2.Retrofit

/**
 * Class containing all app modules with their dependencies
 */

private val remoteDataModule = module {
    single { provideAlbumApi(get()) }
    single { provideArtistApi(get()) }
    single { provideArtistRemoteDataRepository(get()) }
    single { provideAlbumRemoteDataRepository(get()) }
    single { provideAlbumLocalDataRepository(get(), get()) }
}

private val viewModelModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { SearchArtistViewModel(get()) }
    viewModel { ArtistTopAlbumsViewModel(get()) }
    viewModel { AlbumDetailsViewModel(get(), get()) }
}

val baseModules: List<Module> = listOf(roomModule, retrofitNetworkModule)
val appModules: List<Module> = listOf(remoteDataModule, viewModelModule) + baseModules


private fun provideAlbumApi(retrofitInstance: Retrofit): AlbumApi {
    return retrofitInstance.create(AlbumApi::class.java)
}

private fun provideArtistApi(retrofitInstance: Retrofit): ArtistApi {
    return retrofitInstance.create(ArtistApi::class.java)
}

private fun provideArtistRemoteDataRepository(artistApi: ArtistApi): ArtistRemoteDataRepository {
    return ArtistRemoteDataRepository(artistApi)
}

private fun provideAlbumRemoteDataRepository(albumApi: AlbumApi): AlbumRemoteDataRepository {
    return AlbumRemoteDataRepository(albumApi)
}

private fun provideAlbumLocalDataRepository(albumDao: AlbumDao, trackDao: TrackDao): AlbumLocalDataRepository {
    return AlbumLocalDataRepository(albumDao, trackDao)
}
